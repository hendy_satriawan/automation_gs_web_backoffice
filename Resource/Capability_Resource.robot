*** Setting ***
Library    SeleniumLibrary


*** Variables ***
${link}    http://backoffice.guesehat.com/auth/login
${browser}    Chrome
${implicit_wait}    Set Browser Implicit Wait    10s

*** Keywords ***
Buka Browser Guesehat
  ${options}=    Evaluate  sys.modules['selenium.webdriver.chrome.options'].Options()    sys
  Call Method     ${options}    add_argument    --disable-notifications
  ${driver}=    Create Webdriver    Chrome    options=${options}
  Go To    ${link}
  Maximize Browser Window
  Wait Until Page Contains Element    //body[@class='login-page']/div[@class='login-box']//form[@action='http://backoffice.guesehat.com/auth/login']//button[@name='form-submit']
