*** Setting ***
Library    SeleniumLibrary

*** Variables ***
${email_login}    intan.guesehat@gmail.com
${pass_login}   Gue123

*** Keywords ***
Login Web Guesehat
  # input manual popup login & captcha
  # Sleep    30s
  #input email
  Wait Until Page Contains Element    //body[@class='login-page']/div[@class='login-box']//form[@action='http://backoffice.guesehat.com/auth/login']//input[@name='email']
  Input Text    //body[@class='login-page']/div[@class='login-box']//form[@action='http://backoffice.guesehat.com/auth/login']//input[@name='email']    ${email_login}
  #input password
  Wait Until Page Contains Element    //body[@class='login-page']/div[@class='login-box']//form[@action='http://backoffice.guesehat.com/auth/login']//input[@name='password']
  Input Text    //body[@class='login-page']/div[@class='login-box']//form[@action='http://backoffice.guesehat.com/auth/login']//input[@name='password']    ${pass_login}
  # klik tombol sign in
  Click Button    //button[contains(text(),"Sign In")]
  Wait Until Page Contains Element    //div[@class='slimScrollDiv']/section[@class='sidebar']/ul[@class='sidebar-menu']/li[@class='header']
  Wait Until Page Contains Element    //body//div[@class='content-wrapper']/section[2]//h1[.='Welcome, Intan anindyana']
